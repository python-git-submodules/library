class ChunkCreator:
    def create_chunk(self, data_list, size):
        chunk_size = max(1, size)
        # return (data_list[i:i + chunk_size] for i in range(0, len(data_list), chunk_size))
        return [data_list[i:i + chunk_size] for i in range(0, len(data_list), chunk_size)]
